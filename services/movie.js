const MongoLib = require("../lib/mongo");

class MovieService {
  constructor() {
    (this.collection = "movies"), (this.mongoDB = new MongoLib());
  }

  async getMovies() {
    const movies = await this.mongoDB.getAll(this.collection);
    return movies || {};
  }

  async createMovie({ movie }) {
    const createMovieId = await this.mongoDB.create(this.collection, movie);
    return createMovieId || {};
  }

  async getMovie({ movieId }) {
    const movie = await this.mongoDB.get(this.collection, movieId);
    return movie || {};
  }

  async updateMovie({ movieId, movie } = {}) {
    const updateMovieId = await this.mongoDB.update(
      this.collection,
      movieId,
      movie
    );
    return updateMovieId;
  }
}

module.exports = MovieService;
