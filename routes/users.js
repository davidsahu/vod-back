const express = require("express");
const UserService = require("../services/user");

function usersApi(app) {
  const router = express.Router();

  app.use("/api/users", router);

  const userService = new UserService();

  router.get("/", async function(req, res, next) {
    try {
      const users = await userService.getUsers();
      res.status(200).json({
        data: users,
        message: "users listed"
      });
    } catch (error) {
      next(error);
    }
  });

  router.get("/:userId", async function(req, res, next) {
    const { userId } = req.params;
    try {
      const user = await userService.getUser({ userId });
      res.status(200).json({
        data: user,
        message: "user retrived"
      });
    } catch (error) {
      next(error);
    }
  });

  router.post("/createUser", async function(req, res, next) {
    const { body: user } = req;
    try {
      const createUser = await userService.createUser({ user });
      res.status(200).json({
        data: createUser,
        message: "user created"
      });
    } catch (error) {}
  });

  router.put("/:userId", async function(req, res, next) {
    const { userId } = req.params;
    const { body: user } = req;

    try {
      const updateUserId = await userService.updateUser({
        userId,
        user
      });
      res.status(200).json({
        data: updateUserId,
        message: "user updated"
      });
    } catch (error) {
      next(error);
    }
  });

  router.put("/:userId/append/:attr", async function(req, res, next) {
    const { history } = req.body;
    const { attr, userId } = req.params;
    const user = await userService.getUser({ userId });
    let arrayId = [];

    // console.log(history);
    try {
      let $push = {};
      $push[attr] = history;

      let query = {
        $push
      };
      console.log("🤶", user.history);
      if (user.history.length > 0 || user.history !== undefined) {
        user.history.map(({ _id }) => {
          // console.log(_id)
          arrayId.push(_id);
        });

        console.log("arrayId", arrayId);
        if (arrayId.includes(history._id)) {
          console.log("ya existe el id");
        } else {
          console.log("query push", query);
          const appendUserId = new Promise((resolve, reject) => {
            if (resolve) {
              userService.appendUser({
                query,
                userId
              });
              res.status(200).json({
                data: appendUserId,
                message: "document inserted"
              });
            } else console.log(reject);
          });
        }
      } else {
        console.log("query push", query);
        const appendUserId = await userService.appendUser({
          query,
          userId
        });
        res.status(200).json({
          data: appendUserId,
          message: "document inserted"
        });
      }
    } catch (error) {
      next(error);
    }
  });


  router.put("/:userId/editIndex/:attr", async function(req, res, next) {
    const { index } = req.body;
    const { attr, userId } = req.params;

    try {
      let $set = {};
      $set[attr] = index;

      let query = {
        $set
      };

      const editIndexId = await userService.appendUser({
        query,
        userId
      });
      res.status(200).json({
          data: editIndexId,
          message: "index edited"
      })
    } catch (error) {
      next(error);
    }
  });

  router.delete("/:userId", async function(req, res, next) {
    const { userId } = req.params;
    try {
      const deleteUserId = await userService.deleteUser({
        userId
      });
      res.status(200).json({
        data: deleteUserId,
        message: "user deleted"
      });
    } catch (error) {
      next(error);
    }
  });
}

module.exports = usersApi;
