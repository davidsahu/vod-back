const express = require("express");
const MovieService = require("../services/movie");

function moviesApi(app) {
  const router = express.Router();

  app.use("/api/movies", router);

  const movieService = new MovieService();

  router.get("/", async function(req, res, next) {
    try {
      const movies = await movieService.getMovies();
      res.status(200).json({
        data: movies,
        message: "movies listed"
      });
    } catch (error) {
      next(error);
    }
  });

  router.get("/:movieId", async function(req, res, next) {
    const { movieId } = req.params;
    try {
      const movie = await movieService.getMovie({ movieId });
      res.status(200).json({
        data: movie,
        message: "movie retrived"
      });
    } catch (error) {
      next(error);
    }
  });

  router.post("/createMovie", async function(req, res, next) {
    const { body: movie } = req;

    try {
      const createMovie = await movieService.createMovie({ movie });
      res.status(200).json({
        data: createMovie,
        message: "movie created"
      });
    } catch (error) {
      next(error);
    }
  });

  router.put("/:movieId", async function(req, res, next) {
    const { movieId } = req.params;
    const { body: movie } = req;
    try {
      const updateMovieId = await movieService.updateMovie({
        movieId,
        movie
      });
      res.status(200).json({
        data: updateMovieId,
        message: "movie updated"
      });
    } catch (error) {
      next(error);
    }
  });
}

module.exports = moviesApi;
