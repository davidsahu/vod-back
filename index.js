const express = require("express");
const app = express();
const cors = require("cors");

const { config } = require("./config/index");
const moviesApi = require("./routes/movies");
const usersApi = require("./routes/users");

app.use(cors());

app.use(express.json());

moviesApi(app);
usersApi(app);

app.listen(config.port, function() {
  console.log(`listening http://localhost:${config.port}`);
});
